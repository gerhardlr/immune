import { isUndefined } from "util";



class TimeKeeper {
    listeners: any[];
    intervalID: NodeJS.Timeout;
    calibrated_waitID: NodeJS.Timeout;
    constructor() {
        this.listeners = [];
        this.intervalID = setTimeout(()=>{},0);
        this.calibrated_waitID = setTimeout(()=>{},0);
    }

    compile_message():any{
        let minute = new Date(Date.now()).getMinutes();
        let message = {
            minute_type: minute % 6
        }
        return message;
    }

    add(listener: Listener) {
        this.listeners.push(listener);
    }
    remove(listener: Listener) {
        var index = this.listeners.indexOf(listener);
        this.listeners.splice(index, 1);
    }
    remove_all():void{
        this.listeners = [];
    }
    set_regular_timed_events():void{
        this.notify();
        const bounded_callback = this.notify.bind(this);
        this.intervalID = setInterval(bounded_callback,60*1000);
    }
    start(calibrate:boolean=true){
        
        const bounded_callback = this.set_regular_timed_events.bind(this);
        let seconds_remaining = 0;
        if (calibrate){
            let seconds_passed = new Date(Date.now()).getSeconds();
            seconds_remaining = 60 - seconds_passed;
        }
        this.calibrated_waitID = setTimeout(bounded_callback,seconds_remaining*1000);

    }
    stop(){
        clearInterval(this.intervalID)
        clearTimeout(this.calibrated_waitID)
    }
    notify() {
        let message = this.compile_message(); 
        this.listeners.forEach( (listener)=> {
            listener.update(message);
        });
    }
}

interface Listener {
    update(message: any):void;
}

export default TimeKeeper;
export {Listener}; 

