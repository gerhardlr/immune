import {Listener} from '../src/time_keeper'
import TimeKeeper from '../src/time_keeper'

//harness
class Listener_test implements Listener {
    updates: number;
    constructor(){
        this.updates = 0;
    }
    update(message: any): void {
        this.updates +=1;
    }
    
}
let t: TimeKeeper;
beforeEach(()=>t = new TimeKeeper());
afterEach(()=>{
    t.stop();
    t.remove_all();
    jest.clearAllMocks();
})

test('I can add a observer',()=>{
    //Given
    let l = new Listener_test();
    //When
    t.add(l);
    //then
    expect(t.listeners).toHaveLength(1)
})
test('I can subscribe to timed events without calibration',()=>{
    //Given
    jest.useFakeTimers();
    let l = new Listener_test();
    jest.spyOn(l,'update');
    //and
    t.add(l);
    //when I start the clock without calibration
    t.start(false);
    //I expect no updates on my listeners to be called
    expect(l.update).not.toBeCalled()
    //untill a delay of at least 60 seconds
    jest.advanceTimersByTime(60*1000);
    expect(l.update).toBeCalled();
    //and thereafter every at least every 60 seonds I expect a call
    jest.advanceTimersByTime(60*1000);
    expect(l.update).toBeCalled();
});
test('I can subscribe calibrated to nearest minute',()=>{
    //Given
    jest.useFakeTimers();
    let l = new Listener_test();
    jest.spyOn(global.Date, 'now').mockImplementationOnce(() =>
      new Date('2019-05-14T11:00:30.000Z').valueOf()
    );
    jest.spyOn(global,'setTimeout');
    //and
    t.add(l);
    //when
    t.start();
    expect(setTimeout).toBeCalledWith(expect.any(Function),30000)
})
test('I can clear all future binded callbacks',()=>{
    jest.useFakeTimers();
    jest.spyOn(global,'setTimeout');
    jest.spyOn(t,'set_regular_timed_events');
    jest.spyOn(t,'notify');
    jest.spyOn(global.Date, 'now').mockImplementationOnce(() =>
        new Date('2019-05-14T11:00:30.000Z').valueOf()
    );
    //Given a timekeeper that has been started
    t.start()
    //when I stop it
    t.stop()
    //and time to nearest minute has elapsed
    jest.advanceTimersByTime(30*1000);
    //I expect no calibrate callbacks to have been called
    expect(t.set_regular_timed_events).toBeCalledTimes(0)
    //and if at least another minute elapsed
    jest.advanceTimersByTime(60*1000);
    //I expect no set time callbacks to have been called
    expect(t.notify).toBeCalledTimes(0)
});

test('I will be be notified the next minute and thereafter every minute',()=>{
    //Given a timekeeper with a generic listener
    jest.useFakeTimers();
    let l = new Listener_test();
    jest.spyOn(l,'update');
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:00:30.000Z').valueOf()
    );
    jest.spyOn(t,'compile_message');
    jest.spyOn(t,'set_regular_timed_events');
    jest.spyOn(t,'notify');
    //and that listened is scubribed to that timekeepr
    t.add(l);
    //when the timekeeper is started with calibration enabled (default)
    t.start();
    //I expect the timekeeper to calculate the calibrationed delays correctly by means of calling getting the current time
    expect(Date.now).toBeCalledTimes(1);
    //and that after a time elapsed that takes the time to the nearest minute
    jest.advanceTimersByTime(30*1000);
    //I expect a message to be compiled
    expect(t.compile_message).toBeCalledTimes(1)
    //I expect regular events to be initiated
    expect(t.notify).toBeCalledTimes(1)
    //I expect a time message to be calculated by means of getting the current time
    expect(Date.now).toBeCalledTimes(2);
    //Then if the time elapsed by another 5 minutes
    jest.advanceTimersByTime(60*1000*5);
    //I expected to be notified
    expect(t.notify).toBeCalledTimes(6)/*
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:01:00.001Z').valueOf()
    );
    expect(l.update).toBeCalledWith({
        minute_type:0
    });*/
});
test('I will receive a message giving me the minute type',()=>{
    //Given a timekeeper with a generic listener
    jest.useFakeTimers();
    let l = new Listener_test();
    jest.spyOn(l,'update');
    jest.spyOn(global.Date,'now').mockImplementationOnce(()=>
        new Date('2019-05-14T11:00:30.000Z').valueOf()
    );
    //and that listener is scubribed to that timekeeper
    t.add(l);
    //when the timekeeper is started with calibration enabled (default)
    t.start();
    //and that after a time elapsed that takes the time to the nearest minute
    jest.spyOn(global.Date,'now').mockImplementationOnce(()=>
        new Date('2019-05-14T11:01:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(30*1000);
    //I expect the listener to be notified with minute type 1
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:1
    });
    //thereafter if the time elapsed for another minute
    jest.spyOn(global.Date,'now').mockImplementationOnce(()=>
        new Date('2019-05-14T11:02:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(60*1000);
    //I expect the listener to be notified with minute type 2
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:2
    });
    //thereafter if the time elapsed for another 15 minutes
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:17:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(60*1000*15);
    //I expect the listener to be notified with minute type 5
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:5
    });
    //if the time moves to 11:18
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:18:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(60*1000);
    //I expect the listener to be notified with minute type 0
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:0
    });
    //if the time moves to 11:19
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:19:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(60*1000);
    //I expect the listener to be notified with minute type 1
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:1
    });
    //if the time moves to 11:20
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:20:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(60*1000);
    //I expect the listener to be notified with minute type 2
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:2
    });
    //if the time moves to 11:21
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:21:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(60*1000);
    //I expect the listener to be notified with minute type 3
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:3
    });
    //if the time moves to 11:22
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:22:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(60*1000);
    //I expect the listener to be notified with minute type 4
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:4
    });
    //if the time moves to 11:23
    jest.spyOn(global.Date,'now').mockImplementation(()=>
        new Date('2019-05-14T11:23:00.000Z').valueOf()
    );
    jest.advanceTimersByTime(60*1000);
    //I expect the listener to be notified with minute type 5
    expect(l.update).toHaveBeenLastCalledWith({
        minute_type:5
    });


});
test('I can notify two listeners',()=>{
    //Given
    let p = new TimeKeeper();
    let l1 = new Listener_test();
    let l2 = new Listener_test();
    jest.spyOn(l1,'update');
    jest.spyOn(l2,'update');
    p.add(l1);
    p.add(l2);
    //when
    p.notify();
    //then
    expect(l1.update).toBeCalledWith(expect.any(Object));
    expect(l2.update).toBeCalledWith(expect.any(Object));
})

