import LocationDispatcher from '../src/location_dispatcher'
import TimeKeeper from '../src/time_keeper'

test('I can invoke register function on 1st minute',()=>{
    //given a newly created location Disptcher configured on an instance of TimeKeeper (mocked)
    jest.mock('../src/time_keeper')
    const t = new TimeKeeper()
    const l = new LocationDispatcher(t);
    //when the timekeeper signals an event of minute  type 1
    t.notify({
        minute_type:1
    });
    //I expect the register function on the Location Dispatcher to be called
    

});
